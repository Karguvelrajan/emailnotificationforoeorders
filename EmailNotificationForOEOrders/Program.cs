﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using System.IO;
using System.Reflection;

namespace EmailNotificationForOEOrders
{
    class Program
    {
      

        static void Main(string[] args)
        {
            DataTable oeTable = null;
            oeTable = getOEOrders();
            sendMail(oeTable);
        }

        public static DataTable getOEOrders()
        {
            DataTable oeTable = null;
            SqlCommand sqlCmd;
            SqlConnection sqlCnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.Append("SELECT DISTINCT O.ORDERID, C.OEStartDate, CC.ISPARTNER, CU.CUSTOMERID, c.CarrierID, c.statusid,");
            sqlQuery.Append("CC.CustomerContactEmail As  EmailID, CC.CustomerID, CU.CustomerName, CC.CustomerContactName,");
            sqlQuery.Append("DATEDIFF(day, FORMAT(C.OEStartDate, 'dd/MMM/yyyy'), FORMAT(GETDATE(), 'dd/MMM/yyyy')) As difdate  ");
            sqlQuery.Append("FROM [dbo].[Carrier] C INNER JOIN [dbo].[Order] O ");
            sqlQuery.Append("ON C.ORDERID = O.ORDERID INNER JOIN [dbo].[Customer] CU  ON CU.ORDERID = O.ORDERID ");
            sqlQuery.Append("INNER JOIN [dbo].[CustomerContact] CC ON CC.CUSTOMERID = CU.CUSTOMERID ");
            sqlQuery.Append("INNER JOIN [dbo].[UserProfile] U ON U.Email = CC.CustomerContactEmail ");
            sqlQuery.Append("WHERE C.OEStartDate Is Not Null AND DATEDIFF(day, FORMAT(C.OEStartDate, 'dd/MMM/yyyy'), FORMAT(GETDATE(), 'dd/MMM/yyyy')) = 45  ");
            sqlQuery.Append("AND O.OrderType=2  and c.StatusId not in (3,4) and o.StatusId = 2 and cc.IsPartner = 1 ");

            string connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            sqlCnn = new SqlConnection(connStr);
            try
            {
                sqlCnn.Open();
                sqlCmd = new SqlCommand(sqlQuery.ToString(), sqlCnn);
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds);
                sqlCnn.Close();
                sqlCnn.Dispose();
                sqlCmd.Dispose();
                adapter.Dispose();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                return oeTable;
            }
        }

        static void sendMail(DataTable dt)
        {
            // log4net for Error logs
            log4net.Config.BasicConfigurator.Configure();
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
            string to = " ";
            string customerName = "";
            try
            {
                if (dt != null)
                {
                    string from = "notifications@ebenefitsnetwork.com";
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["EmailID"] != DBNull.Value)
                        {
                            to = Convert.ToString(row["EmailID"]);
                            customerName = Convert.ToString(row["CustomerName"]);
                            string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).Replace(@"\bin\Debug", "");
                            string path = Path.Combine(executableLocation, "EmailNotificationsTemplate.html");
                            string mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", Convert.ToString(row["CustomerName"])).Replace("_X", Convert.ToString(row["difdate"])).Replace("_CustomerName", Convert.ToString(row["CustomerName"]));
                            string mailSubject = "Email Notification for OE Orders";
                            mailSendFunctionlity(from, to, "Email Notification for OE Orders", mailBody, customerName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message.ToString(), ex);
            }
        }

        static void mailSendFunctionlity(string from, string to, string subject, string body,string customerName)
        {
            // log4net for Error logs
            log4net.Config.BasicConfigurator.Configure();
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
            // Smtp settings
            SmtpClient client = new SmtpClient();
            // string to = Convert.ToString(row["EmailID"]);
          //  to = "Karguvelrajan.P@gmail.com";
            MailMessage message = new MailMessage(from, to);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            try
            {
                client.Send(message);
                message.Dispose();
                client.Dispose();
            }
            catch (Exception ex)
            {
                message.Dispose();
                client.Dispose();
                log.Error("Customer Name : " + customerName + ", EmailID : " + to + " Error Message: " + ex.Message.ToString(), ex);
            }
        }
    }
}

